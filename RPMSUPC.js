/*
Author: Samuel Gray
Create Date: 06/06/2024
Modified By: Samuel Gray
Modified Date: 11/20/2024

Purpose: The purpose of RPMS_UPC is to delete or add a upc to an item in goRPMS
*/

const fs = pjs.wrap(require('fs'));
const fspath = require('path');
const dayjs = require('dayjs');
const auto = pjs.require("pjslogging/autoLog.js");

async function RPMS_UPC() {

  const currentPath = __dirname + fspath.sep; // C:\tyler\modules\
  const parentPath = fspath.join(currentPath, "..") + fspath.sep; // C:\tyler\modules\gorpms\
  let token = pjs.require(parentPath + "tokenhandler" + fspath.sep + "get_RPMS_API_Token.js");

  const programName = `RPMS_UPC`;

  const currentPort = profound.settings.port;

  let data = fs.readFileSync(parentPath + "RPMS_Dev_URL_Path.json");
  let tokenParm = 0; //Default set to Dev

  let upcTable= "dotti.rmupcupdp";

  //Setting tables
  if(currentPort === 8083){ //PROD
    // data = fs.readFileSync(parentPath + "RPMS_Prod_URL_Path.json"); 
    // tokenParm = 1; 
  }

  auto.autoLog(`${programName} is running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`, programName, 5);
  console.log(`${programName} is running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);


  let subject = `${programName}: Error Detected During Processing`;
  let message = "";

  let reportSummaryVariables = {};
    reportSummaryVariables.delete = 0;
    reportSummaryVariables.post = 0;
    reportSummaryVariables.invalidCallType = [];
    reportSummaryVariables.missingInfo = [];
    reportSummaryVariables.httpErrors = [];
    reportSummaryVariables.flagErrors = [];

  //Calling Token Handler
  auto.autoLog(`Calling token handler API.`, programName, 5);

  token = await token.run(tokenParm); //The 1 here is to tell the token pgm to get PROD token and PROD data! No value results in MainFrame token.
  if(token.errorMsg) {
    message = `An error occurred while grabbing token: ${token.errorMsg}`;
    auto.autoLog(message, programName, 20);
    console.log(message);

    await sendEmail(subject,message);

    console.log(`${programName} is done running on ${currentPort}...${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);
    auto.autoLog(`${programName} is done running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`, programName, 5);
    return;
  }
    
  const apiURLPath = JSON.parse(data);
  const baseUrl = apiURLPath.url;

  auto.autoLog(`goRPMS URL:  ${baseUrl}`, programName, 5);
  console.log(`goRPMS URL:  ${baseUrl}`);

  const upcTableSelectQuery = `SELECT * FROM ${upcTable} WHERE rusent = ''`;

  try {
    auto.autoLog(`Querying ${upcTable} table to grab upc records.`, programName, 5);
    var getUpcRecords = pjs.query(upcTableSelectQuery);

  } catch (err) {
    message = `An error occurred while retreiving upc records from ${upcTable}: ${err}`;
    console.log(message);
    auto.autoLog(message, programName, 20);

    await sendEmail(subject,message);
    
    console.log(`${programName} is done running on ${currentPort}...${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);
    auto.autoLog(`${programName} is done running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`, programName, 5);
    return;
  } 

  console.log(`Records pulled from ${upcTable}: ${getUpcRecords.length}`);
  auto.autoLog(`Records pulled from ${upcTable}: ${getUpcRecords.length}`, programName, 5);

  if(getUpcRecords.length == 0){
    message = `No records found in ${upcTable} :${getUpcRecords.length}`;
    auto.autoLog(message, programName, 20);
    console.log(message);
    
    await sendEmail(subject,message);

    console.log(`${programName} is done running on ${currentPort}...${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);
    auto.autoLog(`${programName} is done running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`, programName, 5);

    return;
  }  

  console.log(`Looping through upc records to update goRPMS`);
  auto.autoLog(`Looping through upc records to update goRPMS`, programName, 5);

  for(let i = 0; i < getUpcRecords.length; i++){
    // console.log(getUpcRecords[i]);

    const itemTemp = Number(getUpcRecords[i].ruitem);
    const itemCode = Number(`${getUpcRecords[i].ruitem}${getUpcRecords[i].ruchkdig}`);
    const callType = getUpcRecords[i].ruaction;
    const upcCode = getUpcRecords[i].ruupc;


    if(callType == 'D'){
      ++reportSummaryVariables.delete;

      await goDelete(token,itemTemp,itemCode,upcCode);

    } else if(callType == 'A'){
      ++reportSummaryVariables.post;

      await goPost(token,itemTemp,itemCode,upcCode);
            
    } else {
      message = `The callType of ${callType} is not valid for item number ${itemCode}.`;
      console.log(message);
      auto.autoLog(message, programName, 20);

      reportSummaryVariables.invalidCallType.push(`itemCode: ${itemCode}, callType: ${callType} `);

      await sendEmail(subject,message);
    }

  } // END OF LOOP

  console.log("Finished looping through upc records.");
  auto.autoLog("Finished looping through upc records.", programName, 5);

  subject = `${programName}: Report Summary`
  const reportSummary = 
    `
    End of ${programName} Processing: Summary Report
    ${currentPort}
    Date: ${dayjs().format('YYYY-MM-DD HH:mm:ss')}
    ------------------------------------------

    Total Records Retrieved from ${upcTable}: ${getUpcRecords.length}
    - Number of Records Created (POST): ${reportSummaryVariables.post}
    - Number of Records Deleted: ${reportSummaryVariables.delete}
  
    Records with Incorrect Call Type: 
    ${reportSummaryVariables.invalidCallType.join("\n")}
      
    Records with Errors During Processing:
    ${reportSummaryVariables.httpErrors.join("\n")}
  
    Records with Errors During Flagging:
    ${reportSummaryVariables.flagErrors.join("\n")}
  
  
    ** In case of any processing errors, an email notification was dispatched to the developers **
  `;

  console.log(reportSummary);

  await sendEmail(subject, reportSummary);

  auto.autoLog(`${programName} is done running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`, programName, 5);
  console.log(`${programName} is done running on  ${currentPort}... ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);




  // Call Type DELETE---------------------------------------------------------------------------------------------------------
  async function goDelete(token,itemTemp,itemCode,upcCode) {
    let errorSubject = `${programName}: Error Detected During Processing`;
    let message = "";
 
    let _upcAPIObject = {
      method: "DELETE",
      uri: baseUrl + `/api/MainframePreviousUpcCodes/${itemCode}/${upcCode}`, //PROD
      headers: { Authorization: "Bearer " + token.value },
      json: true,
      timeout: 5000,
      time: true,
      alwaysReadBody : true,
      statusCode: true,
      responseHeaders: true    
    }; 

    message = `Calling MainframePreviousUpcCodes Delete API for itemCode ${itemCode} and upcCode ${upcCode}.`;
    console.log(message);
    auto.autoLog(message, programName, 5);


    try {
      var upcAPIResult = pjs.httpRequest(_upcAPIObject);
    } catch (err) {      
      message = `Error calling MainframePreviousUpcCodes DELETE API for item code  ${itemCode}. Err: ` + err;
      auto.autoLog(message, programName, 20);
      reportSummaryVariables.httpErrors.push(`Error: ${message}`);
      return sendEmail(errorSubject,message);  
    }

    //retrieve the status code via "responseStatus" and the headers via "responseHeaders"
    let deleteStatus = _upcAPIObject.responseStatus;
 
    if(deleteStatus != 200 && deleteStatus != 201){
      message = `Http DELETE error occurred while calling MainframePreviousUpcCodes DELETE API for item code ${itemCode}.HTTP Status: ${deleteStatus}. Message: ${upcAPIResult.message}`
      auto.autoLog(message, programName, 10);
      console.log(message);
       
      reportSummaryVariables.httpErrors.push(`Error: ${message}`);

      message = `Skipping processing itemCode ${itemCode}...`;

      auto.autoLog(message, programName, 10);
      console.log(message);

      return;
    }

    message = `Successfully deleted itemCode ${itemCode} and upcCode ${upcCode} from goRPMS. Status Code: ${deleteStatus}`;
    console.log(message);
    auto.autoLog(message, programName, 5);
 
    return await updateUpcRecordTable(itemTemp,upcCode);   
  }

  async function goPost(token,itemTemp,itemCode,upcCode){
    let errorSubject = `${programName}: Error Detected During Processing`;
    let message = "";

    let _upcAPIObject = {
      method: "POST",
      uri: baseUrl + "/api/MainframePreviousUpcCodes", //PROD
      headers: { Authorization: "Bearer " + token.value },
      body: {
        itemTemp : itemTemp,
        itemCode : itemCode,
        upcCode : upcCode
      },
      json: true,
      timeout: 5000,
      time: true,
      alwaysReadBody : true,
      statusCode: true,
      responseHeaders: true   

    };

    message = `Calling MainframePreviousUpcCodes POST API for itemCode ${itemCode} and upcCode ${upcCode}.`;
    console.log(message);
    auto.autoLog(message, programName, 5);

     
    try {  
      var upcAPIResult = pjs.httpRequest(_upcAPIObject);
    } catch (err) {
        
      message = `Error calling MainframePreviousUpcCodes POST API for item code  ${itemCode}. Err: ` + err;
  
      auto.autoLog(message, programName, 20);
  
      reportSummaryVariables.httpErrors.push(`Error: ${message}`);
  
      return sendEmail(errorSubject,message);  
    }

    let postStatus = _upcAPIObject.responseStatus;

    if(postStatus != 200 && postStatus != 201){
      message = `Http POST error occurred while calling MainframePreviousUpcCodes POST API for item code ${itemCode}.HTTP Status: ${postStatus}. Message: ${upcAPIResult.message}`
      auto.autoLog(message, programName, 10);
      console.log(message);
        
      console.log(`Detailed error message: ${JSON.stringify(upcAPIResult.modelState)}`);
      // auto.autoLog(`Detailed error message: ${JSON.stringify(putResponse.modelState)}`, programName, 10);

      reportSummaryVariables.httpErrors.push(`Error: ${message}`);

      message = `Skipping processing itemCode ${itemCode}...`;

      auto.autoLog(message, programName, 10);
      console.log(message);

      return;
    }

    message = `Successfully sent itemCode ${itemCode} and upcCode ${upcCode} to goRPMS. Status Code: ${postStatus}`;
    console.log(message);
    auto.autoLog(message, programName, 5);
  
    return await updateUpcRecordTable(itemTemp,upcCode);   
  }

  async function updateUpcRecordTable(itemTemp,upcCode) { 
  
    let errorSubject = `${programName}: Error Detected During Processing`;
    let message = "";
      
    message = `Flagging itemTemp ${itemTemp} and upcCode ${upcCode} in ${upcTable}.`
    console.log(message);
    auto.autoLog(message, programName, 5);

    try {
      var upcUpdateResponse = pjs.query(`UPDATE ${upcTable} SET rusent = 'Y', rusentts = ? WHERE ruitem = ? AND ruupc = ? AND rusent = '' LIMIT 1`,
      [dayjs().format('YYYY-MM-DD HH:mm:ss'), itemTemp, upcCode]);
  
      if (sqlstate != "00000") {
        message = `Sql State error occurred while flagging itemTemp ${itemTemp} and upcCode ${upcCode} in ${upcTable} table. SQL state: ${sqlstate}`;
        console.log(message);
        auto.autoLog(message, programName, 20);
  
        reportSummaryVariables.flagErrors.push(`Error: ${message}`);
        return await sendEmail(subject,message);
      }      
  
    } catch (err) {
      message = `Catch error occurred while flagging itemTemp ${itemTemp} and upcCode ${upcCode} in ${upcTable}: ${err}`;
      console.log(message);
      auto.autoLog(message, programName, 20);
  
      reportSummaryVariables.flagErrors.push(`Error: ${message}`);
  
      return await sendEmail(errorSubject,message);
    }

    message = `Successfully flagged iitemTemp ${itemTemp} and upcCode ${upcCode} in ${upcTable}. SQL State: ${upcUpdateResponse.sqlstate}`;
    auto.autoLog(message, programName, 5);
    console.log(message);  
 
    return;
  }    

  async function sendEmail(subject, message){
    auto.autoLog(`sendEmail is running...`, programName, 5);
    
    
    let emailSend =
      `SNDSMTPEMM RCP((sgray@pwadc.com)) SUBJECT('${subject}') ` +
      `NOTE('${message}')`
    ;
    
    try {
      pjs.runCommand(emailSend);
    }
    catch(err) {
      console.log(`Error sending email: ${err}`);
      auto.autoLog("Error sending email: " +  err, programName, 5);
    
    }    
    return;
  }

  async function delay(ms) {
    auto.autoLog(`In the delay`, programName, 5);
    return new Promise(resolve => setTimeout(resolve, ms));
  }



}

exports.run = RPMS_UPC;
