/*
Author: Samuel Gray
Create Date: 06/06/2024
Modified By: 06/06/2024
Modified Date: 06/06/2024

Purpose: The purpose of RPMS_Delete_Item is delete items from goRPMS
*/

const fs = pjs.wrap(require('fs'));
const fspath = require('path');
const dayjs = require('dayjs');
const auto = pjs.require("pjslogging/autoLog.js");

async function RPMS_Delete_Item() {

    const currentPath = __dirname + fspath.sep; // C:\tyler\modules\
    const parentPath = fspath.join(currentPath, "..") + fspath.sep; // C:\tyler\modules\gorpms\
    let token = pjs.require(parentPath + "tokenhandler" + fspath.sep + "get_RPMS_API_Token.js");

    const programName = `RPMS_Delete_Item`;

    const currentPort = profound.settings.port;
    auto.autoLog("currentPort" + currentPort, programName, 5);


    let data = fs.readFileSync(parentPath + "RPMS_Dev_URL_Path.json");
    let tokenParm = 0; 
    let itemTable = "rminvdelp";

  //Setting tables
   if(currentPort === 8081){ //PROD
        data = fs.readFileSync(parentPath + "RPMS_Prod_URL_Path.json");
        tokenParm = 1;
    }

    auto.autoLog(`${programName} is running on  ${currentPort}... ${dayjs()}`, programName, 5);

    let subject = `${programName}: Error Detected During Processing`;
    let message = "";

    var reportSummaryVariables = {};
    reportSummaryVariables.httpErrors = [];
    reportSummaryVariables.flagErrors = [];

  //Calling Token Handler
  auto.autoLog(`Calling token handler API.`, programName, 5);

  token = await token.run(tokenParm); //The 1 here is to tell the token pgm to get PROD token and PROD data! No value results in MainFrame token.
  if(token.errorMsg) {
        message = `An error occurred while grabbing token: ${token.errorMsg}`;
        auto.autoLog(message, programName, 20);

        await sendEmail(subject,message);
        return;
    }    


    const apiURLPath = JSON.parse(data);
    const baseUrl = apiURLPath.url;
    auto.autoLog(`goRPMS URL:  ${baseUrl}`, programName, 5);


    const itemTableSelectQuery = `SELECT * FROM ${itemTable} WHERE rdsent != 'Y'`;

    try {
        auto.autoLog(`Querying ${itemTable} table to grab deleted records.`, programName, 5);
        var getDltItemsArray = pjs.query(itemTableSelectQuery);

    } catch (err) {
        message = `An error occurred while retreiving delete item records from ${itemTable}: ${err}`;
        auto.autoLog(message, programName, 5);
        await sendEmail(subject,message);
    
        auto.autoLog(`${programName}is done running...${dayjs()}`, programName, 5);
        return;
    } 

    auto.autoLog(`Records pulled from ${itemTable}: ${getDltItemsArray.length}`, programName, 5);

    if(getDltItemsArray.length == 0){
        message = `No records found in ${itemTable} :${getDltItemsArray.length}`;
        auto.autoLog(message, programName, 20);
    
        await sendEmail(subject,message);
    }

    auto.autoLog(`Looping through deleted item records to update goRPMS`, programName, 5);


    for (let i = 0; i < getDltItemsArray.length; i++) {

        const itemCode = Number(`${getDltItemsArray[i].rditem}${getDltItemsArray[i].rdchkdig}`);

        // const itemCode = 6969; // turn on during testing if needed

        let _itemAPIObject = {
            method: "DELETE",
            uri: baseUrl + `/api/MainframeItems`,
            headers: { Authorization: "Bearer " + token.value },
            json: true,
            timeout: 5000,
            time: true,
            statusCode: true,
            responseHeaders: true
        }; 
    
        try {
            auto.autoLog(`Calling MainframeItems DELETE API for item code ${itemCode}`, programName, 5);
            auto.autoLog(_itemAPIObject.uri, programName, 5);

            var deleteItemAPIResult = pjs.httpRequest(_itemAPIObject);
        } catch (err) {
         message = `Error while calling MainframeItems DELETE API for item code ${itemCode}. Error: ${err}`;
            console.log(deleteItemAPIResult);
         auto.autoLog(message, programName, 20);

         reportSummaryVariables.httpErrors.push(`Error: ${message}`);
      
         await sendEmail(subject,message);

         continue;
        }  

        const itemTableUpdateQuery = `UPDATE ${itemTable} SET rdsent = 'Y', rdsentts = '${dayjs().format('YYYY-MM-DD HH:MM:ss')}' WHERE rditem = ${getDltItemsArray[i].rditem} and rdchkdig = ${getDltItemsArray[i].rdchkdig} and rddeldat = ${getDltItemsArray[i].rddeldat} and rddeltim = ${getDltItemsArray[i].rddeltim} LIMIT 1`;

        try{
            auto.autoLog(`Flagging deleted item code ${itemCode} in ${itemTable}.`, programName, 5);
            auto.autoLog(JSON.stringify(itemTableUpdateQuery), programName, 5);

            var flagResponse = pjs.query(itemTableUpdateQuery);

            if (sqlstate != "00000") {
                message = `Sql error while flagging deleted item code ${itemCode} in ${itemTable} table. SQL state: ${sqlstate}`; 
                auto.autoLog(message, programName, 5);
                await sendEmail(subject,message);

                reportSummaryVariables.flagErrors.push(`Error: ${message}`);
                continue;
            }    

        }catch(err){
            message = `Sql catch error while flagging deleted item code ${itemCode} in ${itemTable} table. Error: ${err}`; 
            auto.autoLog(message, programName, 20);
            reportSummaryVariables.flagErrors.push(`Error: ${message}`);
            await sendEmail(subject,message);

            continue;
        }

        auto.autoLog(JSON.stringify(flagResponse), programName, 5);
    }

    auto.autoLog("Finished looping through deleted records.", programName, 5);


    subject = `${programName}: Report Summary`
    const reportSummary = 
    `
    End of ${programName} Processing: Summary Report
    Date: ${dayjs()}
    Total Records Retrieved from ${itemTable}: ${getDltItemsArray.length}
      
    Records with Errors During Processing:
    ${reportSummaryVariables.httpErrors.join("\n")}
  
    Records with Errors During Flagging:
    ${reportSummaryVariables.flagErrors.join("\n")}
  
  
    ** In case of any processing errors, an email notification was dispatched to the developers **
    `
    console.log(reportSummary);

    await sendEmail(subject, reportSummary);

    auto.autoLog(`${programName} is done running on  ${currentPort}... ${dayjs()}`, programName, 5);


    async function sendEmail(subject, message){
        auto.autoLog("sendEmail is running...", programName, 5);

    
        let emailSend =
        `SNDSMTPEMM RCP((sgray@pwadc.com)(as400reports@pwadc.com)) SUBJECT('${subject}') ` +
        `NOTE('${message}')`;
    
        try {
          pjs.runCommand(emailSend);
        }
        catch(err) {
          auto.autoLog("Error sending email:" + err, programName, 5);
        }
    
        return;
    
    }
}


exports.run = RPMS_Delete_Item;
