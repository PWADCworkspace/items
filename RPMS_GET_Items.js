const fs = pjs.wrap(require("fs"));
const fspath = require("path");

async function MF_Items() {
  console.log("RPMS_GET_Items pgm called.");

  // pjs.defineTable("invmstrpms","TYLER/INVMSTRPMS", {update: true, read: true, write: true, userOpen: true, keyed: true});

  const currentPath = __dirname + fspath.sep; // C:\tyler\modules\gorpms\items\
  const parentPath = fspath.join(currentPath, "..") + fspath.sep; // C:\tyler\modules\gorpms\
  let token = pjs.require(parentPath + "tokenhandler" + fspath.sep + "get_RPMS_API_Token.js");

  let data = fs.readFileSync(parentPath + "RPMS_Dev_URL_Path.json"); //TESTING!!!!!!
  let tokenParm = 0;

  //Setting tables
  if(currentPort === 8081){
    data = fs.readFileSync(parentPath + "RPMS_Prod_URL_Path.json");
    tokenParm = 1;
  }

  token = await token.run(tokenParm); //The 1 here is to tell the token pgm to get PROD token and PROD data! No value results in MainFrame token.
  if (token.errorMsg) {
    console.log(token.errorMsg);
    return;
  }

  const apiURLPath = JSON.parse(data);
  const baseUrl = apiURLPath.url;
  console.log("baseUrl:" + baseUrl);

  console.time("Grab all items");

  let recordArray = [];

  // // Call Type GET---------------------------------------------------------------------------------------------------------
  let itemAPIResult = [];
  itemAPIResult = await pjs.httpRequest({
    method: "GET",
    uri: baseUrl + "/api/MainframeItems/" + 0,
    headers: { Authorization: "Bearer " + token.value },
    json: true,
    timeout: 50000,
    time: true
  });

  console.time("running write loop for items");

  // invmstrpms.open();

  // for (let i = 0; i < itemAPIResult.length; i++) {
    for (let i = 0; i < 3000; i++) {
    let size = itemAPIResult[i]["size"].trim();

    let newItem = itemAPIResult[i]["newItem"];
    let newItemValue = 0;
    if (newItem == "true") newItemValue = 1;

    let defaultCaseCost = itemAPIResult[i]["defaultCaseCostIncludesDeliveryFee"];
    let defaultCaseCostValue = 0;
    if (defaultCaseCost == "true") defaultCaseCostValue = 1;

    let catchWeight = itemAPIResult[i]["catchWeight"];
    let defaultCatchWeightValue = 0;
    if (catchWeight == "true") defaultCatchWeightValue = 1;

    let meatOrProduce = itemAPIResult[i]["meatOrProduceItem"];
    let defaultMeatOrProduceValue = 0;
    if (meatOrProduce == "true") defaultMeatOrProduceValue = 1;

    let previewItem = itemAPIResult[i]["previewItem"];
    let defaultPreviewItemValue = 0;
    if (previewItem == "true") defaultPreviewItemValue = 1;

    let longDescription = "";
    if (itemAPIResult[i]["longDescription"] != null) longDescription = itemAPIResult[i]["longDescription"];

    // let deleted = itemAPIResult[i]['deleted'];
    // console.log(deleted);
    // let deletedValue = '';
    // if (deleted != null) deletedValue = 'yes';


    let newValues = {
      // F1: ++F1,
      F1: i,
      F2: itemAPIResult[i]["itemTemp"],
      F3: itemAPIResult[i]["itemCode"],
      F4: itemAPIResult[i]["balanceOnHand"],
      F5: itemAPIResult[i]["weeklyMovement"],
      F6: itemAPIResult[i]["yearlyMovement"],
      F7: itemAPIResult[i]["lastYearsMovement"],
      F8: itemAPIResult[i]["weeksInInventory"],
      F9: newItemValue,
      F10: itemAPIResult[i]["dealDate"],
      F11: itemAPIResult[i]["dealAmt"],
      F12: itemAPIResult[i]["upcCode"],
      F13: itemAPIResult[i]["groupNumber"],
      F14: size,
      F15: itemAPIResult[i]["units"],
      F16: itemAPIResult[i]["description"],
      F17: itemAPIResult[i]["offPack"],
      F18: itemAPIResult[i]["buyerCode"],
      F19: itemAPIResult[i]["deptCode"],
      F20: itemAPIResult[i]["multi1"],
      F21: itemAPIResult[i]["base1"],
      F22: itemAPIResult[i]["percent1"],
      F23: itemAPIResult[i]["multi2"],
      F24: itemAPIResult[i]["base2"],
      F25: itemAPIResult[i]["percent2"],
      F26: itemAPIResult[i]["multi3"],
      F27: itemAPIResult[i]["base3"],
      F28: itemAPIResult[i]["percent3"],
      F29: itemAPIResult[i]["multi4"],
      F30: itemAPIResult[i]["base4"],
      F31: itemAPIResult[i]["percent4"],
      F32: itemAPIResult[i]["multi5"],
      F33: itemAPIResult[i]["base5"],
      F34: itemAPIResult[i]["percent5"],
      F35: itemAPIResult[i]["multi6"],
      F36: itemAPIResult[i]["base6"],
      F37: itemAPIResult[i]["percent6"],
      F38: itemAPIResult[i]["multi7"],
      F39: itemAPIResult[i]["base7"],
      F40: itemAPIResult[i]["percent7"],
      F41: itemAPIResult[i]["multi8"],
      F42: itemAPIResult[i]["base8"],
      F43: itemAPIResult[i]["percent8"],
      F44: itemAPIResult[i]["multi9"],
      F45: itemAPIResult[i]["base9"],
      F46: itemAPIResult[i]["percent9"],
      F47: itemAPIResult[i]["multi10"],
      F48: itemAPIResult[i]["base10"],
      F49: itemAPIResult[i]["percent10"],
      F50: itemAPIResult[i]["multi1C"],
      F51: itemAPIResult[i]["current1"],
      F52: itemAPIResult[i]["percent1C"],
      F53: itemAPIResult[i]["multi2C"],
      F54: itemAPIResult[i]["current2"],
      F55: itemAPIResult[i]["percent2C"],
      F56: itemAPIResult[i]["multi3C"],
      F57: itemAPIResult[i]["current3"],
      F58: itemAPIResult[i]["percent3C"],
      F59: itemAPIResult[i]["multi4C"],
      F60: itemAPIResult[i]["current4"],
      F61: itemAPIResult[i]["percent4C"],
      F62: itemAPIResult[i]["multi5C"],
      F63: itemAPIResult[i]["current5"],
      F64: itemAPIResult[i]["percent5C"],
      F65: itemAPIResult[i]["multi6C"],
      F66: itemAPIResult[i]["current6"],
      F67: itemAPIResult[i]["percent6C"],
      F68: itemAPIResult[i]["multi7C"],
      F69: itemAPIResult[i]["current7"],
      F70: itemAPIResult[i]["percent7C"],
      F71: itemAPIResult[i]["multi8C"],
      F72: itemAPIResult[i]["current8"],
      F73: itemAPIResult[i]["percent8C"],
      F74: itemAPIResult[i]["multi9C"],
      F75: itemAPIResult[i]["current9"],
      F76: itemAPIResult[i]["percent9C"],
      F77: itemAPIResult[i]["multi10C"],
      F78: itemAPIResult[i]["current10"],
      F79: itemAPIResult[i]["percent10C"],
      F80: defaultCatchWeightValue,
      F81: itemAPIResult[i]["privateLabel"],
      F82: itemAPIResult[i]["status"],
      F83: itemAPIResult[i]["created"].slice(0, 10),
      F84: itemAPIResult[i]["modified"].slice(0, 10),
      F85: defaultMeatOrProduceValue,
      F86: defaultPreviewItemValue,
      F87: itemAPIResult[i]["vendorNumber"],
      F88: itemAPIResult[i]["defaultBaseCost"],
      F89: itemAPIResult[i]["defaultCaseCost"],
      F90: itemAPIResult[i]["defaultUnitCost"],
      F91: longDescription,
      // F92: deletedValue,
      F92: "",
      F93: defaultCaseCostValue
    };

    recordArray.push(newValues);
    // invmstrpms.write();

  }
  // invmstrpms.write();
  console.log(recordArray.length);
  
  // Turn off Journalling
  pjs.setConnectAttr(SQL_ATTR_COMMIT, SQL_TXN_NO_COMMIT);

  // pjs.query("INSERT INTO TESTRPMS/INVMSTRPMS SET ?", recordArray, null, null, 10);
  pjs.query("INSERT INTO INVMSTRPMS SET ?", recordArray, null, null, 10); //TESTING!!!!!!

    // invmstrpms.close();

  console.timeEnd("running write loop for items");
  let today = new Date();
  console.log("RPMS_GET_Items pgm finished at " + today + ".");
  console.timeEnd("Grab all items");
}
exports.run = MF_Items;
