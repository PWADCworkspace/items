const fs = pjs.wrap(require("fs"));
const fspath = require("path");

async function singleItemSingleField() {
  console.log("singleItemSingleField starting.");

  // const directPath = "/tyler" + fspath.sep + "modules" + fspath.sep + "gorpms" + fspath.sep;
  const currentPath = __dirname + fspath.sep; // C:\tyler\modules\gorpms\items\
  const parentPath = fspath.join(currentPath, "..") + fspath.sep; // C:\tyler\modules\gorpms\
  let token = pjs.require(parentPath + "tokenhandler" + fspath.sep + "get_RPMS_API_Token.js");

  let data = fs.readFileSync(parentPath + "RPMS_Dev_URL_Path.json"); //TESTING!!!!!!
  let tokenParm = 0;

  //Setting tables
  if(currentPort === 8081){
    data = fs.readFileSync(parentPath + "RPMS_Prod_URL_Path.json");
    tokenParm = 1;
  }

  token = await token.run(tokenParm); //The 1 here is to tell the token pgm to get PROD token and PROD data! No value results in MainFrame token.
  if (token.errorMsg) {
    console.log(token.errorMsg);
    return;
  }

  const apiURLPath = JSON.parse(data);
  const baseUrl = apiURLPath.url;
  console.log("baseUrl:" + baseUrl);

  let itemQueryResult = [];

  //GET DATA FROM RPMSBUYL AS400 TABLE---------------------------------------------------------------------------------------------------------
  try {
    itemQueryResult = pjs.query("Select * from testrpms.invmstflog where mspost != 'Y'");
  } catch (err) {
    console.log(err);
    // errorNotify(err, 20);
    return;
  }

  console.log( itemQueryResult.length + " new records found in TESTRPMS.INVMSTFLOG.");
  // console.log(itemQueryResult);

  const groupByCategory = itemQueryResult.reduce((group, product) => {
    const { msitem } = product;
    group[msitem] = group[msitem] ?? [];
    // group[msitem].push(product.msfield, product.msafter);
    group[msitem].push({
      op: "replace",
      path: "/" + processMSField(product.msfield),
      value: processMSAfter(product.msfield, product.msafter)
    });
    return group;
  }, {});

  // console.log(groupByCategory);

  //  TESTING-----------------------------------------------------------------------------
  console.log(Object.keys(groupByCategory)[1]); //1487
  groupByCategory[1487].forEach((object) => {
    // console.log(object);
    callPatchAPI(1487, object);
  });
  //  TESTING-----------------------------------------------------------------------------

  //  PRODUCTION--------------------------------------------------------------------------
  Object.keys(groupByCategory).forEach((key) => {
    // console.log(key);
    groupByCategory[key].forEach((object) => {
      // console.log(object);
      callPatchAPI(key, object);
    });
  });
  //  PRODUCTION--------------------------------------------------------------------------

  function processMSField(msfieldValue) {
    switch (msfieldValue.trim()) {
      case "VENDNO": //DONE
        msfieldValue = "vendorNumber";
        break;
      case "BALOH": //DONE
        msfieldValue = "balanceOnHand";
        break;
      case "HISTCR": //DONE
        msfieldValue = "weeklyMovement";
        break;
      case "HISTYD": //DONE
        msfieldValue = "yearlyMovement"; // IS THIS ONE RIGHT?????
        break;
      case "SALELY": //DONE
        msfieldValue = "lastYearsMovement"; // IS THIS ONE RIGHT?????
        break;
      case "WKCNT":
        msfieldValue = "weeksInInventory";
        break;

      //AVERAGEWEEKLYMOVEMENT HERE. ASK MATT.
      
      case "NEWITM": //DONE
        msfieldValue = "newItem";
        break;
      case "LBUYDT": //DONE
        msfieldValue = "dealDate"; // IS THIS ONE RIGHT?????
        break;
      case "PMALLW": //DONE
        msfieldValue = "dealAmt"; // IS THIS ONE RIGHT?????
        break;
      case "UPC": //DONE
        msfieldValue = "upcCode";
        break;
      case "ORDHDR": //DONE
        msfieldValue = "groupNumber";
        break;

      // case 'PKSIZE':
      //   msfieldValue = 'status';
      //   break;

      // size

      //units

      case "ITMDSC": //DONE
        msfieldValue = "description";
        break;
      case "IDIMNA": //DONE
        msfieldValue = "longDescription";
        break;
      case "DLTYPE": //DONE
        msfieldValue = "offPack"; // IS THIS ONE RIGHT?????
        break;
      case "BUYER": //DONE
        msfieldValue = "buyerCode";
        break;
      case "DEPT": //DONE
        msfieldValue = "deptCode";
        break;
      case "BSSELL": //DONE
        msfieldValue = "defaultBaseCost";
        break;
      case "SELL":
        msfieldValue = "defaultCaseCost";
        break;
      case "UNITCST": //DONE
        msfieldValue = "defaultUnitCost";
        break;

      // defaultCaseCostIncludesDeliveryFee

      case "MULTI1":
        msfieldValue = "multi1";
        break;
      case "BASE1":
        msfieldValue = "base1";
        break;
      case "PERCENT1":
        msfieldValue = "percent1";
        break;
      case "MULTI2":
        msfieldValue = "multi2";
        break;
      case "BASE2":
        msfieldValue = "base2";
        break;
      case "PERCENT2":
        msfieldValue = "percent2";
        break;
      case "MULTI3":
        msfieldValue = "multi3";
        break;
      case "BASE3":
        msfieldValue = "base3";
        break;
      case "PERCENT3":
        msfieldValue = "percent3";
        break;
      case "MULTI4":
        msfieldValue = "multi4";
        break;
      case "BASE4":
        msfieldValue = "base4";
        break;
      case "PERCENT4":
        msfieldValue = "percent4";
        break;
      case "MULTI5":
        msfieldValue = "multi5";
        break;
      case "BASE5":
        msfieldValue = "base5";
        break;
      case "PERCENT5":
        msfieldValue = "percent5";
        break;
      case "MULTI6":
        msfieldValue = "multi6";
        break;
      case "BASE6":
        msfieldValue = "base6";
        break;
      case "PERCENT6":
        msfieldValue = "percent6";
        break;
      case "MULTI7":
        msfieldValue = "multi7";
        break;
      case "BASE7":
        msfieldValue = "base7";
        break;
      case "PERCENT7":
        msfieldValue = "percent7";
        break;
      case "MULTI8":
        msfieldValue = "multi8";
        break;
      case "BASE8":
        msfieldValue = "base8";
        break;
      case "PERCENT8":
        msfieldValue = "percent8";
        break;
      case "MULTI9":
        msfieldValue = "multi9";
        break;
      case "BASE9":
        msfieldValue = "base9";
        break;
      case "PERCENT9":
        msfieldValue = "percent9";
        break;
      case "MULTI10":
        msfieldValue = "multi10";
        break;
      case "BASE10":
        msfieldValue = "base10";
        break;
      case "PERCENT10":
        msfieldValue = "percent10";
        break;
      case "MULTI1C":
        msfieldValue = "multi1C";
        break;
      case "CURRENT1":
        msfieldValue = "current1";
        break;
      case "PERCENT1C":
        msfieldValue = "percent1C";
        break;
      case "MULTI2C":
        msfieldValue = "multi2C";
        break;
      case "CURRENT2":
        msfieldValue = "current2";
        break;
      case "PERCENT2C":
        msfieldValue = "percent2C";
        break;
      case "MULTI3C":
        msfieldValue = "multi3C";
        break;
      case "CURRENT3":
        msfieldValue = "current3";
        break;
      case "PERCENT3C":
        msfieldValue = "percent3C";
        break;
      case "MULTI4C":
        msfieldValue = "multi4C";
        break;
      case "CURRENT4":
        msfieldValue = "current4";
        break;
      case "PERCENT4C":
        msfieldValue = "percent4C";
        break;
      case "MULTI5C":
        msfieldValue = "multi5C";
        break;
      case "CURRENT5":
        msfieldValue = "current5";
        break;
      case "PERCENT5C":
        msfieldValue = "percent5C";
        break;
      case "MULTI6C":
        msfieldValue = "multi6C";
        break;
      case "CURRENT6":
        msfieldValue = "current6";
        break;
      case "PERCENT6C":
        msfieldValue = "percent6C";
        break;
      case "MULTI7C":
        msfieldValue = "multi7C";
        break;
      case "CURRENT7":
        msfieldValue = "current7";
        break;
      case "PERCENT7C":
        msfieldValue = "percent7C";
        break;
      case "MULTI8C":
        msfieldValue = "multi8C";
        break;
      case "CURRENT8":
        msfieldValue = "current8";
        break;
      case "PERCENT8C":
        msfieldValue = "percent8C";
        break;
      case "MULTI9C":
        msfieldValue = "multi9C";
        break;
      case "CURRENT9":
        msfieldValue = "current9";
        break;
      case "PERCENT9C":
        msfieldValue = "percent9C";
        break;
      case "MULTI10C":
        msfieldValue = "multi10C";
        break;
      case "CURRENT10":
        msfieldValue = "current10";
        break;
      case "PERCENT10C":
        msfieldValue = "percent10C";
        break;
      case "CWMEAT": //DONE
        msfieldValue = "catchWeight";
        break;
      case "PVTLAB": //DONE
        msfieldValue = "privateLabel";
        break;
      case "STATUS": //DONE
        msfieldValue = "status";
        break;
      // case 'meatOrProduceItem':
      //   msfieldValue = 'meatOrProduceItem';
      //   break;
      // case 'previewItem':
      //   msfieldValue = 'previewItem';
      //   break;

      default:
        break;
    }
    return msfieldValue;
  }

  function processMSAfter(msfieldVal, msafterValue) {
    var msAfter = msafterValue.trim();
    switch (msfieldVal.trim()) {
      case !"IDIMNA":
        msAfter = Number(msAfter);
        break;
      case !"ITMDSC":
        msAfter = Number(msAfter);
        break;
      case !"LBUYDT": //last buy date form INVMST???
        msAfter = Number(msAfter);
        break;
      case !"size":
        msAfter = Number(msAfter);
        break;
      case !"offPack":
        msAfter = Number(msAfter);
        break;
      case !"buyerCode":
        msAfter = Number(msAfter);
        break;
      case !"deptCode":
        msAfter = Number(msAfter);
        break;
      case !"privateLabel":
        msAfter = Number(msAfter);
        break;
      case "NEWITM":
        if (msAfter == 1) {
          msAfter = "true";
        } else {
          msAfter = "false";
        }
        break;
      case "HISTYD":
        msAfter = Number(msAfter);
        break;
      case "HISTCR":
        msAfter = Number(msAfter);
        break;
      case "BALOH":
        msAfter = Number(msAfter);
        break;
      case "WKCNT":
        msAfter = Number(msAfter);
        break;
      default:
        break;
    }
    return msAfter;
  }

  // PATCH QUERY CALL---------------------------------------------------------------
  async function callPatchAPI(itemNumber, itemFields) {
    var itemArray = JSON.stringify(itemFields);
      // var itemArray = JSON.stringify([{"op":"replace","path":"/status","value":""}]);

    // console.log(itemFields); //{ op: 'replace', path: '/balanceOnHand', value: 32 }
    console.log(itemNumber); //1487
    console.log(itemArray); //[{"op":"replace","path":"/status","value":""}]

    await pjs.httpRequest({
      method: 'PATCH',
      uri: baseUrl + '/api/MainframeItems/' + itemNumber + '/0', //NOTE: When do I need to do a /1 vs /0 in this and does it matter?
      headers: {
        'Authorization': "Bearer " + token.value,
        'Accept': 'application/json'
      },
      body: itemArray,
      json: true,
      timeout: 5000,
      time: true
    });
  }
}
exports.run = singleItemSingleField;
