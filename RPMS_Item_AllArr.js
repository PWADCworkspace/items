const fs = pjs.wrap(require("fs"));
const fspath = require("path");
const https = require("follow-redirects").https;

async function ItemsAll() {
  console.log("RPMS_ItemsAll pgm called.");

  pjs.defineTable("invmstlo2","INVMSTLO2", {update: true, read: true, write: true, userOpen: true, keyed:true});

  // const directPath = "/tyler" + fspath.sep + "modules" + fspath.sep + "gorpms" + fspath.sep;

  const currentPath = __dirname + fspath.sep; // C:\tyler\modules\gorpms\items\
  const parentPath = fspath.join(currentPath, "..") + fspath.sep; // C:\tyler\modules\gorpms\
  let token = pjs.require(parentPath + "tokenhandler" + fspath.sep + "get_RPMS_API_Token.js");

  let data = fs.readFileSync(parentPath + "RPMS_Dev_URL_Path.json"); //TESTING!!!!!!
  let tokenParm = 0;

  //Setting tables
  if(currentPort === 8081){
    data = fs.readFileSync(parentPath + "RPMS_Prod_URL_Path.json");
    tokenParm = 1;
  }

  token = await token.run(tokenParm); //The 1 here is to tell the token pgm to get PROD token and PROD data! No value results in MainFrame token.
  if (token.errorMsg) {
    console.log(token.errorMsg);
    return;
  }

  const apiURLPath = JSON.parse(data);
  const baseUrl = apiURLPath.url;
  console.log("baseUrl:" + baseUrl);

  //GET DATA FROM invmstlog AS400 TABLE---------------------------------------------------------------------------------------------------------
  let itemQueryResult = [];
  const itemTableQuery = "Select * from invmstlog where mspost != 'Y'";
  try {
    itemQueryResult = pjs.query(itemTableQuery);
  } catch (err) {
    console.log(err);
    // errorNotify(err, 20);
    return;
  }
// IF NO RECORDS ARE FOUND CONSOLE LOG SUCH AND QUIT
  if (itemQueryResult.length == 0) {
    console.log(itemQueryResult.length + " new records found in INVMSTLOG.");
    return;
  }

  //GET ITEM NUMBERS FROM RPMS FOR COMPARISON.----------------------------------------------------------------------------------------------------
  var getItemNumbers = await pjs.httpRequest({
    method: "GET",
    // uri: "https://devapi.gorpms.com/api/MainframeItems/GetItemCodes/0", //CHANGE LATER FOR PROD!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    uri: baseUrl + "/api/MainframeItems/GetItemCodes/0",
    headers: {
      Authorization: "Bearer " + token.value,
      Accept: "application/json"
    },
    json: true,
    timeout: 5000,
    time: true
  });

  console.log("getItemNumbers");

  // Open invmstlog to flag records as complete in updateItemsRecordsTable() once they are wrtten to RPMS
  invmstlo2.open();

  // LOOP THROUGH REACH ITEM TO DEFINE ALL OF ITS FIELDS AND WRITE EACH RECORD TO RPMS-------------------------------------------------------------
  for (let i = 0; i < itemQueryResult.length; i++) {
    // for (let i = 0; i < 4; i++) {
    let itemTempValue = itemQueryResult[i]["lgitmcde"];
    let itemCodeValue = itemQueryResult[i]["msitem"];
    let callType = itemQueryResult[i]["msevent"];

    let newItemValue = itemQueryResult[i]["lgnewitm"];
    let newItem = false;
    if (newItemValue == 1) newItem = true;

    let dealDateValue = itemQueryResult[i]["lglbuydt"];
    if (dealDateValue == 0) {
      dealDate = '';
    }else {
      dealDate = dealDateValue.toString();
    }
    console.log(dealDate);
    
    let dealAmt = parseFloat(itemQueryResult[i]["lgpmallw"]).toFixed(4);
    let sizeValue = itemQueryResult[i]["lgpksize"];
    sizeValue = sizeValue.trim();

    let privateLabel = itemQueryResult[i]["lgpvtlab"];
    let statusVal = itemQueryResult[i]["lgstatus"];
    let offPack = itemQueryResult[i]["lgdltype"];
    let buyerCode = itemQueryResult[i]["lgbuyer"];
    let deptCode = itemQueryResult[i]["lgdept"];
    let description = itemQueryResult[i]["lgitmdsc"];
    let vendorNumber = itemQueryResult[i]["lgvendno"];
    let balanceOnHand = itemQueryResult[i]["lgbaloh"];
    let weeklyMovement = itemQueryResult[i]["lghistcr"];
    let yearlyMovement = itemQueryResult[i]["lghistyd"];
    let weeksInInventoryValue = itemQueryResult[i]["lgwkcnt"];
    let averageWeeklyMovementValue = 53; //???? // ask matt

    let upcCode = itemQueryResult[i]["lgupc"];
    let groupNumber = itemQueryResult[i]["lgordhdr"];
    let lastYearsMovementValue = itemQueryResult[i]["lgsalely"];

    let unitsValue = sizeValue.substr(0, sizeValue.indexOf("/"));
    unitsValue = Number(unitsValue);

    let longDescription = itemQueryResult[i]["lgitmdesc"];
    let defaultBaseCost = parseFloat(itemQueryResult[i]["lgbssell"]).toFixed(4);
    let defaultCaseCost = parseFloat(itemQueryResult[i]["lgsell"]).toFixed(4);
    let defaultUnitCost = parseFloat(defaultCaseCost / unitsValue).toFixed(4);

    let defaultCatchWeightValue = itemQueryResult[i]["lgcwmeat"];
    let catchWeight = false;
    if (defaultCatchWeightValue == "C" || "c") catchWeight = true;

    let meatOrProduceItemValue = itemQueryResult[i]["lgmetpro"];
    let meatOrProduceItem = false; 
    if (meatOrProduceItemValue == 1) meatOrProduceItem = true;

    let previewItemValue = itemQueryResult[i]["lgpreview"];
    let previewItem = false; 
    if (previewItemValue == 1) previewItem = true;

    let b1srp = itemQueryResult[i]["lgb1bsrp"];
    b1srp = b1srp.trim();
    let multi1 = Number(b1srp.substring(0, 2));
    let base1 = b1srp.substring(2, 7) / 100;
    base1 = base1.toFixed(2);
    let percent1 = itemQueryResult[i]["lgb1bspc"];
    percent1 = Math.round(percent1 * 100);

    let b2srp = itemQueryResult[i]["lgb2bsrp"];
    b2srp = b2srp.trim();
    let multi2 = Number(b2srp.substring(0, 2));
    let base2 = b2srp.substring(2, 7) / 100;
    base2 = base2.toFixed(2);
    let percent2 = itemQueryResult[i]["lgb2bspc"];
    percent2 = Math.round(percent2 * 100);

    let b3srp = itemQueryResult[i]["lgb3bsrp"];
    b3srp = b3srp.trim();
    let multi3 = Number(b3srp.substring(0, 2));
    let base3 = b3srp.substring(2, 7) / 100;
    base3 = base3.toFixed(2);
    let percent3 = itemQueryResult[i]["lgb3bspc"];
    percent3 = Math.round(percent3 * 100);

    let b4srp = itemQueryResult[i]["lgb4bsrp"];
    b4srp = b4srp.trim();
    let multi4 = Number(b4srp.substring(0, 2));
    let base4 = b4srp.substring(2, 7) / 100;
    base4 = base4.toFixed(2);
    let percent4 = itemQueryResult[i]["lgb4bspc"];
    percent4 = Math.round(percent4 * 100);

    let b5srp = itemQueryResult[i]["lgb5bsrp"];
    b5srp = b5srp.trim();
    let multi5 = Number(b5srp.substring(0, 2));
    let base5 = b5srp.substring(2, 7) / 100;
    base5 = base5.toFixed(2);
    let percent5 = itemQueryResult[i]["lgb5bspc"];
    percent5 = Math.round(percent5 * 100);

    let b6srp = itemQueryResult[i]["lgb6bsrp"];
    b6srp = b6srp.trim();
    let multi6 = Number(b6srp.substring(0, 2));
    let base6 = b6srp.substring(2, 7) / 100;
    base6 = base6.toFixed(2);
    let percent6 = itemQueryResult[i]["lgb6bspc"];
    percent6 = Math.round(percent6 * 100);

    let b7srp = itemQueryResult[i]["lgb7bsrp"];
    b7srp = b7srp.trim();
    let multi7 = Number(b7srp.substring(0, 2));
    let base7 = b7srp.substring(2, 7) / 100;
    base7 = base7.toFixed(2);
    let percent7 = itemQueryResult[i]["lgb7bspc"];
    percent7 = Math.round(percent7 * 100);

    let b8srp = itemQueryResult[i]["lgb8bsrp"];
    b8srp = b8srp.trim();
    let multi8 = Number(b8srp.substring(0, 2));
    let base8 = b8srp.substring(2, 7) / 100;
    base8 = base8.toFixed(2);
    let percent8 = itemQueryResult[i]["lgb8bspc"];
    percent8 = Math.round(percent8 * 100);

    let b9srp = itemQueryResult[i]["lgb9bsrp"];
    b9srp = b9srp.trim();
    let multi9 = Number(b9srp.substring(0, 2));
    let base9 = b9srp.substring(2, 7) / 100;
    base9 = base9.toFixed(2);
    let percent9 = itemQueryResult[i]["lgb9bspc"];
    percent9 = Math.round(percent9 * 100);

    let b0srp = itemQueryResult[i]["lgb0bsrp"];
    b0srp = b0srp.trim();
    let multi10 = Number(b0srp.substring(0, 2));
    let base10 = b0srp.substring(2, 7) / 100;
    base10 = base10.toFixed(2);
    let percent10 = itemQueryResult[i]["lgb0bspc"];
    percent10 = Math.round(percent10 * 100);

    let b1bsrp = itemQueryResult[i]["lgb1srp"];
    b1bsrp = b1bsrp.trim();
    let multi1C = Number(b1bsrp.substring(0, 2));
    let current1 = b1bsrp.substring(2, 7) / 100;
    current1 = current1.toFixed(2);
    let percent1C = itemQueryResult[i]["lgb1mkup"];
    percent1C = Math.round(percent1C * 100);

    let b2bsrp = itemQueryResult[i]["lgb2srp"];
    b2bsrp = b2bsrp.trim();
    let multi2C = Number(b2bsrp.substring(0, 2));
    let current2 = b2bsrp.substring(2, 7) / 100;
    current2 = current2.toFixed(2);
    let percent2C = itemQueryResult[i]["lgb2mkup"];
    percent2C = Math.round(percent2C * 100);

    let b3bsrp = itemQueryResult[i]["lgb3srp"];
    b3bsrp = b3bsrp.trim();
    let multi3C = Number(b3bsrp.substring(0, 2));
    let current3 = b3bsrp.substring(2, 7) / 100;
    current3 = current3.toFixed(2);
    let percent3C = itemQueryResult[i]["lgb3mkup"];
    percent3C = Math.round(percent3C * 100);

    let b4bsrp = itemQueryResult[i]["lgb4srp"];
    b4bsrp = b4bsrp.trim();
    let multi4C = Number(b4bsrp.substring(0, 2));
    let current4 = b4bsrp.substring(2, 7) / 100;
    current4 = current4.toFixed(2);
    let percent4C = itemQueryResult[i]["lgb4mkup"];
    percent4C = Math.round(percent4C * 100);

    let b5bsrp = itemQueryResult[i]["lgb5srp"];
    b5bsrp = b5bsrp.trim();
    let multi5C = Number(b5bsrp.substring(0, 2));
    let current5 = b5bsrp.substring(2, 7) / 100;
    current5 = current5.toFixed(2);
    let percent5C = itemQueryResult[i]["lgb5mkup"];
    percent5C = Math.round(percent5C * 100);

    let b6bsrp = itemQueryResult[i]["lgb6srp"];
    b6bsrp = b6bsrp.trim();
    let multi6C = Number(b6bsrp.substring(0, 2));
    let current6 = b6bsrp.substring(2, 7) / 100;
    current6 = current6.toFixed(2);
    let percent6C = itemQueryResult[i]["lgb6mkup"];
    percent6C = Math.round(percent6C * 100);

    let b7bsrp = itemQueryResult[i]["lgb7srp"];
    b7bsrp = b7bsrp.trim();
    let multi7C = Number(b7bsrp.substring(0, 2));
    let current7 = b7bsrp.substring(2, 7) / 100;
    current7 = current7.toFixed(2);
    let percent7C = itemQueryResult[i]["lgb7mkup"];
    percent7C = Math.round(percent7C * 100);

    let b8bsrp = itemQueryResult[i]["lgb8srp"];
    b8bsrp = b8bsrp.trim();
    let multi8C = Number(b8bsrp.substring(0, 2));
    let current8 = b8bsrp.substring(2, 7) / 100;
    current8 = current8.toFixed(2);
    let percent8C = itemQueryResult[i]["lgb8mkup"];
    percent8C = Math.round(percent8C * 100);

    let b9bsrp = itemQueryResult[i]["lgb9srp"];
    b9bsrp = b9bsrp.trim();
    let multi9C = Number(b9bsrp.substring(0, 2));
    let current9 = b9bsrp.substring(2, 7) / 100;
    current9 = current9.toFixed(2);
    let percent9C = itemQueryResult[i]["lgb9mkup"];
    percent9C = Math.round(percent9C * 100);

    let b0bsrp = itemQueryResult[i]["lgb0srp"];
    b0bsrp = b0bsrp.trim();
    let multi10C = Number(b0bsrp.substring(0, 2));
    let current10 = b0bsrp.substring(2, 7) / 100;
    current10 = current10.toFixed(2);
    let percent10C = itemQueryResult[i]["lgb0mkup"];
    percent10C = Math.round(percent10C * 100);

    // let itemObject = JSON.stringify({
    let itemObject = {
      itemTemp: itemTempValue,
      itemCode: itemCodeValue,
      vendorNumber: vendorNumber,
      balanceOnHand: balanceOnHand,
      weeklyMovement: weeklyMovement,
      yearlyMovement: yearlyMovement,
      lastYearsMovement: lastYearsMovementValue,
      weeksInInventory: weeksInInventoryValue,
      averageWeeklyMovement: averageWeeklyMovementValue,
      newItem: newItem,
      dealDate: dealDate,
      dealAmt: dealAmt,
      upcCode: upcCode,
      groupNumber: groupNumber,
      size: sizeValue,
      units: unitsValue,
      description: description,
      longDescription: longDescription,
      offPack: offPack,
      buyerCode: buyerCode,
      deptCode: deptCode,
      defaultBaseCost: defaultBaseCost,
      defaultCaseCost: defaultCaseCost,
      defaultUnitCost: defaultUnitCost,
      defaultCaseCostIncludesDeliveryFee: false, //???? // ask matt
      multi1: multi1,
      base1: base1,
      percent1: percent1,
      multi2: multi2,
      base2: base2,
      percent2: percent2,
      multi3: multi3,
      base3: base3,
      percent3: percent3,
      multi4: multi4,
      base4: base4,
      percent4: percent4,
      multi5: multi5,
      base5: base5,
      percent5: percent5,
      multi6: multi6,
      base6: base6,
      percent6: percent6,
      multi7: multi7,
      base7: base7,
      percent7: percent7,
      multi8: multi8,
      base8: base8,
      percent8: percent8,
      multi9: multi9,
      base9: base9,
      percent9: percent9,
      multi10: multi10,
      base10: base10,
      percent10: percent10,
      multi1C: multi1C,
      current1: current1,
      percent1C: percent1C,
      multi2C: multi2C,
      current2: current2,
      percent2C: percent2C,
      multi3C: multi3C,
      current3: current3,
      percent3C: percent3C,
      multi4C: multi4C,
      current4: current4,
      percent4C: percent4C,
      multi5C: multi5C,
      current5: current5,
      percent5C: percent5C,
      multi6C: multi6C,
      current6: current6,
      percent6C: percent6C,
      multi7C: multi7C,
      current7: current7,
      percent7C: percent7C,
      multi8C: multi8C,
      current8: current8,
      percent8C: percent8C,
      multi9C: multi9C,
      current9: current9,
      percent9C: percent9C,
      multi10C: multi10C,
      current10: current10,
      percent10C: percent10C,
      catchWeight: catchWeight,
      privateLabel: privateLabel,
      status: statusVal,
      meatOrProduceItem: meatOrProduceItem, 
      previewItem: previewItem 
    };
    // });

    console.log(itemCodeValue);

    if (
      callType == "A" &&
      getItemNumbers.itemCodes.includes(itemCodeValue) == true
    ) {
      console.log("calling goPut for item " + itemCodeValue);
      goPut(token, itemObject, itemCodeValue);
    } else if (
      callType == "A" &&
      getItemNumbers.itemCodes.includes(itemCodeValue) == false
    ) {
      console.log("calling goPost for item " + itemCodeValue);
      goPost(token, itemObject, itemCodeValue);
    } else if (
      callType == "D" &&
      getItemNumbers.itemCodes.includes(itemCodeValue) == true
    ) {
      console.log("calling goDelete for item " + itemCodeValue);
      goDelete(token, itemCodeValue);
    } else if (
      callType == "D" &&
      getItemNumbers.itemCodes.includes(itemCodeValue) == false
    ) {
      console.log("item " + itemCodeValue + " already does not exist in RPMS.");
      updateItemsRecordsTable(itemCodeValue);
    } else goBadMethod(itemCodeValue);
  }
  
  // Close invmstlog after flagging records in updateItemsRecordsTable() once they are wrtten to RPMS
  invmstlo2.close();
  // END PROCESS HERE---------------------------------------------------------------------------------------------------------

  // Call Type DELETE---------------------------------------------------------------------------------------------------------
  async function goDelete(token, inItemCodeValue) {
    console.log(
      "RPMS_Items_AllArr Call type: DEL. for item: " + inItemCodeValue
    );

    try {
      await pjs.httpRequest({
        method: "DELETE",
        uri: baseUrl + "/api/MainframeItems/" + inItemCodeValue + "/0",
        headers: { Authorization: "Bearer " + token.value },
        json: true,
        timeout: 5000,
        time: true
      });

      console.log('DELETE successful for item: ' + inItemCodeValue);
      //UPDATE THE COMPARE TABLE IN 400 FOR EACH ITEM AS IT HAS FINISHED.----------------------------------------------------
      await updateItemsRecordsTable(inItemCodeValue);
    } catch (err) {
      console.log("Error DELETING Item Info. Err: " + err);
      // errorNotify(err, 20);
    }
  }

  // CAll Type POST---------------------------------------------------------------------------------------------------------
  async function goPost(token, inItemObject, inItemCodeValue) {
    console.log(
      "RPMS_Items_AllArr Call type: POST. for item: " + inItemCodeValue
    );
    // console.log(inItemObject); 

    try {
      await pjs.httpRequest({
        method: "POST",
        uri: baseUrl + "/api/MainframeItems",
        headers: { Authorization: "Bearer " + token.value },
        body: {
          itemTemp: inItemObject.itemTemp,
          itemCode: inItemObject.itemCode,
          vendorNumber: inItemObject.vendorNumber,
          balanceOnHand: inItemObject.balanceOnHand,
          weeklyMovement: inItemObject.weeklyMovement,
          yearlyMovement: inItemObject.yearlyMovement,
          lastYearsMovement: inItemObject.lastYearsMovement,
          weeksInInventory: inItemObject.weeksInInventory,
          averageWeeklyMovement: inItemObject.averageWeeklyMovement,
          newItem: inItemObject.newItem,
          dealDate: inItemObject.dealDate,
          dealAmt: inItemObject.dealAmt,
          upcCode: inItemObject.upcCode,
          groupNumber: inItemObject.groupNumber,
          size: inItemObject.size,
          units: inItemObject.units,
          description: inItemObject.description,
          longDescription: inItemObject.longDescription,
          offPack: inItemObject.offPack,
          buyerCode: inItemObject.buyerCode,
          deptCode: inItemObject.deptCode,
          defaultBaseCost: inItemObject.defaultBaseCost,
          defaultCaseCost: inItemObject.defaultCaseCost,
          defaultUnitCost: inItemObject.defaultUnitCost,
          defaultCaseCostIncludesDeliveryFee: false,
          multi1: inItemObject.multi1,
          base1: inItemObject.base1,
          percent1: inItemObject.percent1,
          multi2: inItemObject.multi2,
          base2: inItemObject.base2,
          percent2: inItemObject.percent2,
          multi3: inItemObject.multi3,
          base3: inItemObject.base3,
          percent3: inItemObject.percent3,
          multi4: inItemObject.multi4,
          base4: inItemObject.base4,
          percent4: inItemObject.percent4,
          multi5: inItemObject.multi5,
          base5: inItemObject.base5,
          percent5: inItemObject.percent5,
          multi6: inItemObject.multi6,
          base6: inItemObject.base6,
          percent6: inItemObject.percent6,
          multi7: inItemObject.multi7,
          base7: inItemObject.base7,
          percent7: inItemObject.percent7,
          multi8: inItemObject.multi8,
          base8: inItemObject.base8,
          percent8: inItemObject.percent8,
          multi9: inItemObject.multi9,
          base9: inItemObject.base9,
          percent9: inItemObject.percent9,
          multi10: inItemObject.multi10,
          base10: inItemObject.base10,
          percent10: inItemObject.percent10,
          multi1C: inItemObject.multi1C,
          current1: inItemObject.current1,
          percent1C: inItemObject.percent1C,
          multi2C: inItemObject.multi2C,
          current2: inItemObject.current2,
          percent2C: inItemObject.percent2C,
          multi3C: inItemObject.multi3C,
          current3: inItemObject.current3,
          percent3C: inItemObject.percent3C,
          multi4C: inItemObject.multi4C,
          current4: inItemObject.current4,
          percent4C: inItemObject.percent4C,
          multi5C: inItemObject.multi5C,
          current5: inItemObject.current5,
          percent5C: inItemObject.percent5C,
          multi6C: inItemObject.multi6C,
          current6: inItemObject.current6,
          percent6C: inItemObject.percent6C,
          multi7C: inItemObject.multi7C,
          current7: inItemObject.current7,
          percent7C: inItemObject.percent7C,
          multi8C: inItemObject.multi8C,
          current8: inItemObject.current8,
          percent8C: inItemObject.percent8C,
          multi9C: inItemObject.multi9C,
          current9: inItemObject.current9,
          percent9C: inItemObject.percent9C,
          multi10C: inItemObject.multi10C,
          current10: inItemObject.current10,
          percent10C: inItemObject.percent10C,
          catchWeight: inItemObject.catchWeight,
          privateLabel: inItemObject.privateLabel,
          status: inItemObject.statusVal,
          meatOrProduceItem: meatOrProduceItem,
          previewItem: previewItem
        },
        json: true,
        timeout: 5000,
        time: true
      });

      console.log('POST successful for item: ' + inItemCodeValue);
      //UPDATE THE COMPARE TABLE IN 400 FOR EACH ITEM AS IT HAS FINISHED.----------------------------------------------------
      await updateItemsRecordsTable(inItemCodeValue);
    } catch (err) {
      console.log("Error POSTING Item Info. Err: " + err);
      // errorNotify(err, 20);
    }
  }

  // CAll Type PUT---------------------------------------------------------------------------------------------------------
  async function goPut(token, inItemObject, inItemCodeValue) {
    console.log(
      "RPMS_Items_AllArr Call type: PUT. for item: " + inItemCodeValue
    );
    // console.log(inItemObject);

    try {
      await pjs.httpRequest({
        method: "PUT",
        uri: baseUrl + "/api/MainframeItems/" + inItemCodeValue,
        headers: { Authorization: "Bearer " + token.value },
        body: {
          itemTemp: inItemObject.itemTemp,
          itemCode: inItemObject.itemCode,
          vendorNumber: inItemObject.vendorNumber,
          balanceOnHand: inItemObject.balanceOnHand,
          weeklyMovement: inItemObject.weeklyMovement,
          yearlyMovement: inItemObject.yearlyMovement,
          lastYearsMovement: inItemObject.lastYearsMovement,
          weeksInInventory: inItemObject.weeksInInventory,
          averageWeeklyMovement: inItemObject.averageWeeklyMovement,
          newItem: inItemObject.newItem,
          dealDate: inItemObject.dealDate,
          dealAmt: inItemObject.dealAmt,
          upcCode: inItemObject.upcCode,
          groupNumber: inItemObject.groupNumber,
          size: inItemObject.size,
          units: inItemObject.units,
          description: inItemObject.description,
          longDescription: inItemObject.longDescription,
          offPack: inItemObject.offPack,
          buyerCode: inItemObject.buyerCode,
          deptCode: inItemObject.deptCode,
          defaultBaseCost: inItemObject.defaultBaseCost,
          defaultCaseCost: inItemObject.defaultCaseCost,
          defaultUnitCost: inItemObject.defaultUnitCost,
          defaultCaseCostIncludesDeliveryFee: false,
          multi1: inItemObject.multi1,
          base1: inItemObject.base1,
          percent1: inItemObject.percent1,
          multi2: inItemObject.multi2,
          base2: inItemObject.base2,
          percent2: inItemObject.percent2,
          multi3: inItemObject.multi3,
          base3: inItemObject.base3,
          percent3: inItemObject.percent3,
          multi4: inItemObject.multi4,
          base4: inItemObject.base4,
          percent4: inItemObject.percent4,
          multi5: inItemObject.multi5,
          base5: inItemObject.base5,
          percent5: inItemObject.percent5,
          multi6: inItemObject.multi6,
          base6: inItemObject.base6,
          percent6: inItemObject.percent6,
          multi7: inItemObject.multi7,
          base7: inItemObject.base7,
          percent7: inItemObject.percent7,
          multi8: inItemObject.multi8,
          base8: inItemObject.base8,
          percent8: inItemObject.percent8,
          multi9: inItemObject.multi9,
          base9: inItemObject.base9,
          percent9: inItemObject.percent9,
          multi10: inItemObject.multi10,
          base10: inItemObject.base10,
          percent10: inItemObject.percent10,
          multi1C: inItemObject.multi1C,
          current1: inItemObject.current1,
          percent1C: inItemObject.percent1C,
          multi2C: inItemObject.multi2C,
          current2: inItemObject.current2,
          percent2C: inItemObject.percent2C,
          multi3C: inItemObject.multi3C,
          current3: inItemObject.current3,
          percent3C: inItemObject.percent3C,
          multi4C: inItemObject.multi4C,
          current4: inItemObject.current4,
          percent4C: inItemObject.percent4C,
          multi5C: inItemObject.multi5C,
          current5: inItemObject.current5,
          percent5C: inItemObject.percent5C,
          multi6C: inItemObject.multi6C,
          current6: inItemObject.current6,
          percent6C: inItemObject.percent6C,
          multi7C: inItemObject.multi7C,
          current7: inItemObject.current7,
          percent7C: inItemObject.percent7C,
          multi8C: inItemObject.multi8C,
          current8: inItemObject.current8,
          percent8C: inItemObject.percent8C,
          multi9C: inItemObject.multi9C,
          current9: inItemObject.current9,
          percent9C: inItemObject.percent9C,
          multi10C: inItemObject.multi10C,
          current10: inItemObject.current10,
          percent10C: inItemObject.percent10C,
          catchWeight: inItemObject.catchWeight,
          privateLabel: inItemObject.privateLabel,
          status: inItemObject.statusVal,
          meatOrProduceItem: meatOrProduceItem,
          previewItem: previewItem
        },
        json: true,
        timeout: 5000,
        time: true
      });

      console.log('PUT successful for item: ' + inItemCodeValue);
      //UPDATE THE COMPARE TABLE IN 400 FOR EACH ITEM AS IT HAS FINISHED.----------------------------------------------------
      await updateItemsRecordsTable(inItemCodeValue);
    } catch (err) {
      console.log("Error running PUT Item Info. Err: " + err);
      // errorNotify(err, 20);
    }
  }

  // ERROR MESSAGE HANDLE---------------------------------------------------------------------------------------------------------
  function goBadMethod(inItemCodeValue) {
    error =
      "Method call for RPMS_ItemsAll API was not valid, check the CallType being sent in invmstlog for item " +
      inItemCodeValue +
      ".";
    console.log(error);
    // errorNotify(error, 20);
    return;
  }

  function updateItemsRecordsTable(itemCode) {
    console.log("Updating invmstlog to flag imported records.");

    var localTimeStamp = pjs.char(pjs.timestamp(), "*iso");
    var timeStamp = localTimeStamp.substring(11, 19);
    timeStamp = timeStamp.replace(/-|:/g, "");
    var dateStamp = localTimeStamp.substring(0, 10);
    dateStamp = dateStamp.replace(/-|:/g, "");

    invmstlo2.getRecord(itemCode);
    if (invmstlo2.found()) {
      mspost = "Y";
      msdates = dateStamp;
      mstimes = timeStamp;
      invmstlo2.update();
      console.log("updateQuery ran for item: " + itemCode + ".");
      return;
    }
    else {
      console.log('No record found for item: ' + itemCode + ', or there was an error writting the record.');
      return;
    }
  }
}
exports.run = ItemsAll;

// function errorNotify(error, severity) {
//   //severity   action
//   // 10        console.log
//   // 20        email + console.log
//   // 30        send message to qsysopr on 400 + email + console.log

//   let messagePrefix =
//     pjs.char(pjs.timestamp(), "*iso") + "RPMS_ItemsAll API Error: ";
//   console.log(messagePrefix + error);

//   pjs.define("message", { type: "char", length: 512 });
//   message = messagePrefix + error;
//   // /*
//     if (severity >= 10)
//     {
//       pjs.call("SNDEMAIL",  message);
//     }

//     if (severity >= 20)
//     {
//       pjs.call("SNDMESSAGE",  message);
//     }
// // */
// }
